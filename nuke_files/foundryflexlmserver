#!/bin/bash
# Copyright 2012 The Foundry
#
# chkconfig: 345 99 06
# description: Foundry FLEXlm Server

#NOTE : The Foundry Server Utility uses these 2 elements to locate the server daemon, 
#and the floating license files in use if you alter these ensure they still point to 
#the FLT FLEXlm bin tools dir and the license directory in use
TOOL_DIR=/usr/local/foundry/LicensingTools7.1
FnLicDir=/usr/local/foundry/FLEXlm

FnGetFloatingLicenseFiles()
{
  fileList=""
  dirList=$(echo $1 | tr ":" "\n")
  for dirItem in ${dirList}
  do
    for file in ${dirItem}/*
    do
      #Locate only license files containing floating licenses to serve
      vendorLine=`grep "VENDOR foundry" $file | sed 's/^ *//g'`
      if [ "${vendorLine:0:1}" = "#" ]
      then
        #Commented out, skip it
        vendorLine=""
      fi
      if [ "$vendorLine" != "" ]
      then
        clientLine=`grep "USE_SERVER" $file | sed 's/^ *//g'`
        if [ "$clientLine" != "" ]
        then
          clientLine="${clientLine:0:1}" 
          if [ "${clientLine}" == 'U' ]
          then
            #Client license, and not commented out, so ignore this file
            vendorLine=""
          fi
        fi
      fi
      if [ "$vendorLine" != "" ]
      then
        if [ "$fileList" != "" ]
        then
          fileList=$fileList":"
        fi
        fileList=$fileList$file
      fi
    done
  done
  if [ "$fileList" == "" ]
  then
    echo $1
  else
    echo $fileList
  fi
  return 0
}

BIN_DIR=${TOOL_DIR}/bin/FLEXlm
LIC_FILES=`FnGetFloatingLicenseFiles ${FnLicDir}`
LOG_DIR=${FnLicDir}/log
DEF_ULIMIT=`ulimit -n`
FOUNDRY_ULIMIT=10240

case "$1" in
'start')
  LMGRD_IS_UP=`ps -ef | grep lmgrd.foundry | grep -v grep`
  if [ "$LMGRD_IS_UP" != "" ]
  then
    #lmgrd is already running, call reload to start our vendor with its licenses
    $0 reload
  else
    if [ ${DEF_ULIMIT} -lt ${FOUNDRY_ULIMIT} ]
    then
      echo -e "\033[33mDefault descriptor limit ${DEF_ULIMIT} insufficient, increasing to ${FOUNDRY_ULIMIT}\033[0m"
      ulimit -n ${FOUNDRY_ULIMIT}
      TEST_ULIMIT=`ulimit -n`
      if [ ${TEST_ULIMIT} -eq ${FOUNDRY_ULIMIT} ]
      then
        echo -e "\033[32mSuccessfully increased descriptor limit to ${TEST_ULIMIT} \033[0m"
      else
        echo -e "\033[31mFailed to increase descriptor limit, currently set to ${TEST_ULIMIT} \033[0m"
      fi
    fi
    echo -e "\033[32mStarting lmgrd Server\033[0m"
    ${BIN_DIR}/lmgrd.foundry -c ${LIC_FILES} -l ${LOG_DIR}/foundry.log -local 2>&1 >> ${LOG_DIR}/boot.log
    ${BIN_DIR}/lmutil lmdiag -n -c ${LIC_FILES}/ 2>&1 >> ${LOG_DIR}/boot.log
  fi
  ;;

'stop')
  echo -e "\033[32mStopping lmgrd Server\033[0m"
  ${BIN_DIR}/lmutil lmdown -q -c ${LIC_FILES} -vendor foundry 2>&1 >> ${LOG_DIR}/boot.log << End_Of_Session
  1
End_Of_Session
  ;;

'reload')
  echo -e "\033[32mReloading FLEXlm licenses\033[0m"
  ${BIN_DIR}/lmutil lmreread -c ${LIC_FILES} -vendor foundry -all 2>&1 >> ${LOG_DIR}/boot.log
  ;;

'restart')
  $0 stop
  $0 start
  ;;

*)
   echo "usage: $0 {start|stop|reload|restart}"
   ;;
esac
