#!/bin/bash

if [ `id -u` -ne 0 ]
then
  echo -e "\033[31mYou must run this script as root.\033[0m"
  exit 1
fi


umask 022

FnRLMLicDir=/usr/local/foundry/RLM
FnFLEXLicDir=/usr/local/foundry/FLEXlm

#NOTE : The Foundry Server Utility uses this element to locate the server daemon, 
#if you alter this ensure it still points to the FLT tools dir 
TOOL_DIR=/usr/local/foundry/LicensingTools7.1

RLM_LIC_DIR=${FnRLMLicDir}
FLEX_LIC_DIR=${FnFLEXLicDir}
BIN_DIR=${TOOL_DIR}/bin
RLM_BIN_DIR=${TOOL_DIR}/bin/RLM
FLEX_BIN_DIR=${TOOL_DIR}/bin/FLEXlm
DOC_DIR=${TOOL_DIR}/docs
RLM_LOG_DIR=${FnRLMLicDir}/log
FLEX_LOG_DIR=${FnFLEXLicDir}/log
INIT_DIR=/etc/init.d

#Surely we should stop it if we are about to replace it?
if [ -f ${INIT_DIR}/foundryrlmserver ]
then
  echo -e "\033[31mCurrently installed files\033[0m"
  echo -e "\033[31m/etc/init.d/foundryrlmserver\033[0m"
  echo -e "\033[31m \033[0m"
  echo -e "\033[31mThere appears to be an installation of The Foundry Licensing Tools\033[0m"
  echo -e "\033[31mon this machine. Please uninstall it before continuing by\033[0m"
  echo -e "\033[31mrunning /usr/local/foundry/LicensingTools<version-major>.<version-minor>/uninstall.sh\033[0m"
  exit 1
fi
if [ -f ${INIT_DIR}/foundryflexlmserver ]
then
  echo -e "\033[31mCurrently installed files\033[0m"
  echo -e "\033[31m/etc/init.d/foundryflexlmserver\033[0m"
  echo -e "\033[31m \033[0m"
  echo -e "\033[31mThere appears to be an installation of The Foundry FLEXlm Tools\033[0m"
  echo -e "\033[31mon this machine. Please uninstall it before continuing by\033[0m"
  echo -e "\033[31mrunning /usr/local/foundry/FLEXlmTools5.0/bin/uninstall.sh\033[0m"
  exit 1
fi

echo
echo -e "\033[33mPlease note : The Foundry Licensing Tools are only for installation on the license server machine.\033[0m"
echo -e "\033[33mIt should not be installed on any of the client-side machines.\033[0m"


echo -e "\033[32mInstalling The Foundry Licensing Tools 7.1\033[0m"
#echo ${BIN_DIR}
#if [ ! -d ${BIN_DIR} ]
#then
#  mkdir -p ${BIN_DIR}
#fi

echo ${RLM_BIN_DIR}
if [ ! -d ${RLM_BIN_DIR} ]
then
  mkdir -p ${RLM_BIN_DIR}
fi

echo ${FLEX_BIN_DIR}
if [ ! -d ${FLEX_BIN_DIR} ]
then
  mkdir -p ${FLEX_BIN_DIR}
fi

if [ ! -d ${DOC_DIR} ]
then
  mkdir -p ${DOC_DIR}
fi

if [ ! -d ${RLM_LIC_DIR} ]
then
  mkdir -p ${RLM_LIC_DIR}
fi

if [ ! -d ${FLEX_LIC_DIR} ]
then
  mkdir -p ${FLEX_LIC_DIR}
fi

if [ ! -d ${RLM_LOG_DIR} ]
then
  mkdir -p ${RLM_LOG_DIR}
fi

if [ ! -d ${FLEX_LOG_DIR} ]
then
  mkdir -p ${FLEX_LOG_DIR}
fi

cp ./rlm.foundry ./rlmutil ./foundry.set ${RLM_BIN_DIR}
chown root:root ${RLM_BIN_DIR}/*
chmod 755 ${RLM_BIN_DIR}/*

cp ./lmgrd.foundry ./lmutil ./foundry ${FLEX_BIN_DIR}
chown root:root ${FLEX_BIN_DIR}/*
chmod 755 ${FLEX_BIN_DIR}/*

cp ./FoundryLicenseUtility ./uninstall.sh ${TOOL_DIR}
chown root:root ${TOOL_DIR}/*
chmod 755 ${TOOL_DIR}/*

cp ./foundry.log ${RLM_LOG_DIR}
chown root:root ${RLM_LOG_DIR}
chmod -R 666 ${RLM_LOG_DIR}

cp ./foundry.log ${FLEX_LOG_DIR}
chown root:root ${FLEX_LOG_DIR}
chmod -R 666 ${FLEX_LOG_DIR}

chmod -R 777 ${RLM_LIC_DIR}
chmod -R 777 ${FLEX_LIC_DIR}

cp ./foundryrlmserver ${INIT_DIR}
chown root:root ${INIT_DIR}/foundryrlmserver
chmod 755 ${INIT_DIR}/foundryrlmserver

cp ./foundryflexlmserver ${INIT_DIR}
chown root:root ${INIT_DIR}/foundryflexlmserver
chmod 755 ${INIT_DIR}/foundryflexlmserver

cp -R ./docs/* ${DOC_DIR}
chown root:root ${DOC_DIR}/*
chmod 666 ${DOC_DIR}/*

# Very important, switch to root dir and run the startup scripts from there
#otherwise rlm checks the launch location of the scripts and finds the rlm
#daemon there and launches that one instead of the one we have just installed - RMCE
cd /

# Should probably use lsb_release here but older dists may not have it
if [ -f /etc/redhat-release ]
  then
    echo -e "\033[33mSetting foundry rlm license server to run at startup\033[0m"
    /sbin/chkconfig --level 335 foundryrlmserver on
    #Script reports this too : echo -e "\033[33mStarting Foundry RLM Server\033[0m"
    /etc/init.d/foundryrlmserver start

    echo -e "\033[33mSetting foundry flexlm license server to run at startup\033[0m"
    /sbin/chkconfig --level 335 foundryflexlmserver on
    #Script reports this too : echo -e "\033[33mStarting Foundry FLEXlm Server\033[0m"
    /etc/init.d/foundryflexlmserver start

elif [ -f /etc/debian_version ]
  then
    echo -e "\033[33mSetting foundry rlm license server to run at startup\033[0m"
    /usr/sbin/update-rc.d foundryrlmserver defaults 
    #Script reports this too : echo -e "\033[33mStarting Foundry RLM Server\033[0m"
    /etc/init.d/foundryrlmserver start

    echo -e "\033[33mSetting foundry flexlm license server to run at startup\033[0m"
    /usr/sbin/update-rc.d foundryflexlmserver defaults 
    #Script reports this too : echo -e "\033[33mStarting Foundry FLEXlm Server\033[0m"
    /etc/init.d/foundryflexlmserver start

elif [ -f /etc/SuSE-release ]
  then
    echo -e "\033[33mSetting foundry rlm license server to run at startup\033[0m"
    /sbin/chkconfig --level 335 foundryrlmserver on
    #Script reports this too : echo -e "\033[33mStarting Foundry RLM Server\033[0m"
    /etc/init.d/foundryrlmserver start

    echo -e "\033[33mSetting foundry flexlm license server to run at startup\033[0m"
    /sbin/chkconfig --level 335 foundryflexlmserver on
    #Script reports this too : echo -e "\033[33mStarting Foundry FLEXlm Server\033[0m"
    /etc/init.d/foundryflexlmserver start

else
  echo -e "\033[31mERROR : I don't know how to set rlm.foundry and lmgrd.foundry to run at startup. Please
  consult your OS documentation.\033[0m"
fi

echo -e "\033[32mDone.\033[0m"


