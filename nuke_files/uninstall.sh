#!/bin/bash

FnRLMLicDir=/usr/local/foundry/RLM
FnFLEXLicDir=/usr/local/foundry/FLEXlm
TOOL_DIR=/usr/local/foundry/LicensingTools7.1
RLM_LIC_DIR=${FnRLMLicDir}
RLM_LOG_DIR=${FnRLMLicDir}/log
FLEX_LIC_DIR=${FnFLEXLicDir}
FLEX_LOG_DIR=${FnFLEXLicDir}/log
BIN_DIR=${TOOL_DIR}/bin
DOC_DIR=${TOOL_DIR}/docs
INIT_DIR=/etc/init.d

remove_startup()
{
  echo -e "\033[32mStopping Foundry Servers.....\033[0m"
  /etc/init.d/foundryrlmserver stop

  /etc/init.d/foundryflexlmserver stop

  # Should probably use lsb_release here but older dists may not have it
  if [ -f /etc/redhat-release ]
  then
    /sbin/chkconfig --del foundryrlmserver
    /sbin/chkconfig --del foundryflexlmserver
  elif [ -f /etc/debian_version ]
  then
    /usr/sbin/update-rc.d -f foundryrlmserver remove 
    /usr/sbin/update-rc.d -f foundryflexlmserver remove 
  elif [ -f /etc/SuSE-release ]
  then
    /sbin/chkconfig --del foundryrlmserver
    /sbin/chkconfig --del foundryflexlmserver
  else
    echo -e "\033[31mERROR : I don't know how to stop foundry license server running at startup. Please\033[0m"
    echo -e "\033[31mconsult your OS docs. I have removed the startup script.\033[0m"
  fi
  echo -e "\033[32mStopping RLM Web Server.\033[0m"
  killall -s SIGTERM rlm.foundry 
  rm -f /etc/init.d/foundryrlmserver
  rm -f /etc/init.d/foundryflexlmserver
}

if [ `id -u` -ne 0 ]
then
  echo -e "\033[31mYou must run this script as root.\033[0m"
  exit 1
fi

# TODO: Preserve licenses?  Where?
echo
echo
echo -e "\033[32mUninstalling The Foundry Licensing Tools 7.1\033[0m"
echo
echo -e "\033[33m*** WARNING ***\033[0m"
echo -e "\033[33mThis script will stop The Foundry RLM and FLEXLm license servers and remove them from this machine\033[0m"
echo -e "\033[33mThis script will preserve your licenses and log files.\033[0m"
echo -e "\033[33mAs a result not all directories will be removed.\033[0m"
echo
echo -n "Do you really want to continue <yes/no> "
read REPLY
if [[ $REPLY == "Y" || $REPLY == "YES" || $REPLY == "Yes" || $REPLY == "yes" || $REPLY == "y" ]]
then
  echo
  #No need to check, remove startup shuts them down regardless
  # Stop the server
  #if [ `/usr/bin/pgrep rlm.foundry` ]
  #  then
  #    echo -e "\033[32mStopping the RLM server\033[0m"
  #    /etc/init.d/foundryrlmserver stop
  #fi
  #if [ `/usr/bin/pgrep lmgrd.foundry` ]
  #  then
  #    echo -e "\033[32mStopping the FLEXlm server\033[0m"
  #    /etc/init.d/foundryflexlmserver stop
  #fi
  # Remove foundryrlmserver and foundryflexlmserver from the startup list
  echo
  echo -e "\033[32mRemoving startup scripts\033[0m"
  remove_startup

  # Remove the rest of everything
  echo
  echo -e "\033[32mRemoving installed files\033[0m"
  rm -f ${TOOL_DIR}/bin/RLM/rlm.foundry
  rm -f ${TOOL_DIR}/bin/RLM/rlmutil
  rm -f ${TOOL_DIR}/bin/RLM/foundry.set
  rm -f ${TOOL_DIR}/bin/FLEXlm/lmgrd.foundry
  rm -f ${TOOL_DIR}/bin/FLEXlm/foundry
  rm -f ${TOOL_DIR}/bin/FLEXlm/lmutil
  rm -f ${TOOL_DIR}/uninstall.sh
  rm -f ${TOOL_DIR}/FoundryLicenseUtility
  if [ "$(ls -A ${TOOL_DIR}/bin/FLEXlm)" ]
  then
    echo -e "\033[33mThe following items were not installed by FLT and remain in ${TOOL_DIR}/bin/FLEXlm\033[0m"
    /usr/bin/find ${TOOL_DIR}/bin/FLEXlm
  else
    rm -r ${TOOL_DIR}/bin/FLEXlm
  fi
  if [ "$(ls -A ${TOOL_DIR}/bin/RLM)" ]
  then
    echo -e "\033[33mThe following items were not installed by FLT and remain in ${TOOL_DIR}/bin/RLM\033[0m"
    /usr/bin/find ${TOOL_DIR}/bin/RLM
  else
    rm -r ${TOOL_DIR}/bin/RLM
  fi

  if [ "$(ls -A ${TOOL_DIR}/bin)" ]
  then
    echo
    echo -e "\033[33m${TOOL_DIR}/bin is not empty so not removing\033[0m"
  else
    echo
    echo -e "\033[32mRemoving ${TOOL_DIR}/bin\033[0m"
    rm -rf ${TOOL_DIR}/bin
  fi

  # *.pdf will remove the FLT.pdf and the FLEX End user pdf
  rm -f ${DOC_DIR}/*.pdf
  rm -f ${DOC_DIR}/RLM_Enduser.html
  rm -rf ${DOC_DIR}/rlm_enduser_images 
  if [ `ls ${DOC_DIR}` ]
  then
    echo
    echo -e "\033[32m${DOC_DIR} is not empty so not removing\033[0m"
  else
    rm -rf ${DOC_DIR}
  fi

  # Report back what's left
  echo
  echo -e "\033[32mThe following files and directories remain.\033[0m"
  echo
  /usr/bin/find ${FnRLMLicDir}
  /usr/bin/find ${FnFLEXLicDir}
  /usr/bin/find ${TOOL_DIR}
else
  exit
fi

