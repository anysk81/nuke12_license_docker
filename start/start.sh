#!/bin/bash

CURRENT_ID=`cat /etc/hostname`

echo -e "Prepare"

cd /usr/lib/x86_64-linux-gnu

ln -s ./libidn2.so.0 ./libidn.so.11

echo -e "Inctall"

cd /home/nuke_files

/home/nuke_files/install.sh

echo -e "Stopping"

/usr/local/foundry/LicensingTools7.1/FoundryLicenseUtility -s stop -t RLM

echo -e "Cracking"

sed -i "s/MAC_ADDRESS/${CURRENT_ID}/g" /home/nuke_files/license/xf_foundry.lic

cp /home/nuke_files/license/xf_foundry.lic /usr/local/foundry/RLM/xf_foundry.lic
/bin/cp -rf /home/nuke_files/license/rlm.foundry /usr/local/foundry/LicensingTools7.1/bin/RLM/rlm.foundry

chmod 777 /usr/local/foundry/LicensingTools7.1/bin/RLM/rlm.foundry
chmod 777 /usr/local/foundry/RLM/xf_foundry.lic

echo -e "Starting"

/usr/local/foundry/LicensingTools7.1/FoundryLicenseUtility -s start -t RLM

echo -e "Done"

tail -F anything