FROM ubuntu:20.04

RUN apt update && apt install -qqy net-tools

RUN mkdir /home/nuke_files
RUN mkdir /home/nuke_files/license
RUN mkdir /home/nuke_files/start

COPY nuke_files/ /home/nuke_files/

COPY start/ /home/nuke_files/start

COPY license/ /home/nuke_files/license/

RUN chmod 777 -R /home/nuke_files/license

RUN chmod 777 /home/nuke_files/start/start.sh
RUN chmod 777 /home/nuke_files/install.sh

CMD ["/home/nuke_files/start/start.sh"]